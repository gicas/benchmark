function results_ca = ...
    benchmark_symp(f, t_span, iv, methods, steps_vec)
% BENCHMARK_SYMP() checks whether given methods are symplectic
%   BENCHMARK_SYMP(F, T_SPAN, INIT_VAL, METHODS, STEPS_VEC) tests symplecticity
%   condition ||X'*J*X - J|| = 0 for solution X obtained by each method in METHODS
%   for differential equation y''=F() on T_SPAN with steps in STEPS_VEC.
%
%   BENCHMARK_SYPM returns a cell array with the vector of steps and
%   vector of errors in every row.

[m, n] = size(f(0));
if m ~= n
    error('Function f should produce a square matrix');
end
I = eye(m, n);
O = zeros(m, n);
J = [[O, I]; ...
    [-I, O]];

n_methods = size(methods, 1);
len_steps = length(steps_vec);
errors = zeros(n_methods, len_steps);

results_ca = cell(n_methods, 2);

for m = 1:n_methods
    current_method = methods{m, 1};
    for s = 1:len_steps
        n_steps = round(steps_vec(s));
        X = current_method(f, t_span, iv, n_steps);
        if isvector(iv)
            errors(m, s) = norm(X'*J*X-iv'*J*iv);
        else
            errors(m, s) = norm(X'*J*X-J, 2);
        end
    end
    results_ca{m, 1} = steps_vec;
    results_ca{m, 2} = errors(m,:);
end
end