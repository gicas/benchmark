function results_ca = ...
    benchmark_param(g, t_span, iv_a, iv_b, methods, ref_solver, steps_amb, ...
    param_vec, varargin)
% BENCHMARK_PARAM compares how methods' performance depends on parameter.
%   RESULTS = BENCHMARK_PARAM(g, t_span, init_val, methods, ref_solver,...
%   steps_amb, param_vec, varargin) runs BENCHMARK_METHODS for each parameter
%   in PARAM_VEC.
%
%   BENCHMARK_PARAM returns a cell array in which every row contains the vector
%   of parameters and method's vector of errors.
%
%   G(P, VARARGIN) should return a function F(VARARGIN) that depends on the
%   paramater P.

t0 = t_span(1);
tf = t_span(end);
if steps_amb > 1
    n_steps = steps_amb;
elseif steps_amb <= 1
    n_steps = (tf - t0) / steps_amb;
end

n_methods = size(methods, 1);
n_param = length(param_vec);

errors = zeros(n_methods, n_param);
results_ca = cell(n_methods, 2);

for idx = 1:n_param
    p = param_vec(idx);
    disp(strcat('Benchmarking with parameter p = ', num2str(p)));
    f = g(p, varargin{:});
    results = benchmark_methods(f, t_span, iv_a, iv_b, methods, ref_solver, n_steps);
    errors(:, idx) = cell2mat(results(:, 2));
end
for m = 1:n_methods
    results_ca{m, 1} = param_vec; % x = parameters
    results_ca{m, 2} = errors(m,:); % y = norm-2 of error
end

% end of function
end