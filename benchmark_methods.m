function results_ca = ...
    benchmark_methods(f, t_span, iv_a, iv_b, methods, ref_solver, steps_vec)
% BENCHMARK_METHODS compares a given set of integrators.
%   RESULTS = BENCHMARK_METHODS(f, t_span, init_val, methods, ref_solver,...
%   steps_vec) compares performance of a set of METHODS vs.
%   the solution of a DE y''=F() with IV
%   obtained by REF_SOLVER with different number of steps from STEPS_VEC.
%
%   BENCHMARK_METHODS returns a cell array in which every row contains
%   method's cost, norm-2 error and the last solution.

n_methods = size(methods, 1);

% 3 columns: x=cost, y=diff, result
results_ca = cell(n_methods, 3);

n_steps_ref = max(steps_vec*1.2);
reference = ref_solver(f, t_span, [iv_a;iv_b], n_steps_ref);

for m = 1:n_methods
    current_method = methods{m, 1};
    costs = steps_vec; % will be x-axis
    errors = zeros(size(steps_vec)); % will be y-axis
    for s = 1:length(steps_vec)
        n_steps = steps_vec(s);
        result = current_method(f, t_span, iv_a, iv_b, n_steps);
        errors(s) = norm(reference-result, 2);
    end
    costs = costs .* methods{m, 2};
    
    % This order follows common MATLAB's plot() specification
    results_ca{m, 1} = costs; % x = steps * cost
    results_ca{m, 2} = errors; % y = norm-2 of error
    results_ca{m, 3} = result; % the results with most steps
end